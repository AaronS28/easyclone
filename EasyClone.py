from EasyClone_ui import *
from PyQt5.QtWidgets import *
from subprocess import run, PIPE


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

	currentToCloneItem = None
	currentFromCloneItem = None
	allDrivesList = []

	def __init__(self, *args, **kwargs):
		QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
		self.setupUi(self)
		self.getStorageDevice()
		self.AcceptButton.clicked.connect(self.doClone)
		self.RefreshButton.clicked.connect(self.getStorageDevice)
		self.FromCloneList.itemSelectionChanged.connect(self.fromCloneListSelectionChange)
		self.ToCloneList.itemSelectionChanged.connect(self.toCloneListSelecionChange)


	def getStorageDevice(self):
		self.ToCloneList.clear()
		self.FromCloneList.clear()
		command = ['lsblk -l -p -n -o NAME,MOUNTPOINT | grep \ \/']
		output = run(command, shell=True, stdout=PIPE)

		output_string = output.stdout.decode('utf-8')
		output_string = output_string.strip()

		results = output_string.split('\n')
		allDrives = []
		for device in results:
			item = self.findDriveAndLabels(device)
			allDrives.append(item)
			deviceLabel = device[device.rfind('/'):len(device)]
			fromItem = QListWidgetItem(deviceLabel)
			self.FromCloneList.addItem(fromItem)
			toItem = QListWidgetItem(deviceLabel)
			self.ToCloneList.addItem(toItem)
		self.set_allDrivesList(allDrives)

	# definicion de eventos
	
	# definicion de delegados
	def doClone(self):
		if self.get_currentToCloneItem() != None and self.get_currentFromCloneItem() != None and self.get_currentToCloneItem() != self.get_currentFromCloneItem():
			if self.MakeImgCheckBox.isChecked():
				self.makeImageDump()
			else:
				self.makeDriveDump()


	def fromCloneListSelectionChange(self):
		self.set_currentFromCloneItem(self.FromCloneList.currentRow())

	def toCloneListSelecionChange(self):
		self.set_currentToCloneItem(self.ToCloneList.currentRow())

	def logEventinView(self, logEvent):
		logText = self.LogTextView.toPlainText()
		logText = logText + '\n' + logEvent
		self.LogTextView.setPlainText(logText)

	def findDriveAndLabels(self, drive):
		driveAndLabel = []
		whiteSpacePos = drive.find(' ')
		driveAndLabel.append(drive[0:whiteSpacePos])
		label = drive[whiteSpacePos+1:len(drive)]
		label = label.replace(' ', '\\ ')
		driveAndLabel.append(label)
		return driveAndLabel

	def makeImageDump(self):
		self.logEventinView("Desmontando dispositivos ")
		self.logEventinView("Dispostivos desmontados")
		self.logEventinView("Iniciando Clonacion...")

		toDiskDrive = self.get_allDrivesList()[self.get_currentToCloneItem()]
		fromDiskDrive = self.get_allDrivesList()[self.get_currentFromCloneItem()]
		fromDiskDriveLabel = fromDiskDrive[1]
		fromDiskDriveLabel = fromDiskDriveLabel[fromDiskDriveLabel.rfind('/'):len(fromDiskDriveLabel)]
		fromDiskDriveLabel = fromDiskDriveLabel.replace(' ', '_')
			
		command = ['umount ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)
		command = ['umount ' + fromDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)

		command = ['mkdir ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)
		command = ['mount ' + toDiskDrive[0] + ' ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)

		command = ['dc3dd if=' + fromDiskDrive[0] + ' of='+ toDiskDrive[1] + fromDiskDriveLabel + '.img hash=md5']
		run(command, shell=True, stdout=PIPE)
		self.logEventinView("Clonacion Lista")


		self.logEventinView("Montando dispositivos...")
		command = ['umount ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)
		command = ['mount --options=ro ' + toDiskDrive[0] + ' ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)


		command = ['mkdir ' + fromDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)
		command = ['mount --options=ro ' + fromDiskDrive[0] + ' ' + fromDiskDrive[1]]
		print(command)
		run(command, shell=True, stdout=PIPE)

		self.logEventinView("Listo")
		return

	def makeDriveDump(self):
		self.logEventinView("Desmontando dispositivos ")
		self.logEventinView("Dispostivos desmontados")
		self.logEventinView("Iniciando Clonacion...")

		toDiskDrive = self.get_allDrivesList()[self.get_currentToCloneItem()]
		fromDiskDrive = self.get_allDrivesList()[self.get_currentFromCloneItem()]
		fromDiskDriveLabel = fromDiskDrive[1]
		fromDiskDriveLabel = fromDiskDriveLabel[fromDiskDriveLabel.rfind('/'):len(fromDiskDriveLabel)]
		fromDiskDriveLabel = fromDiskDriveLabel.replace(' ', '_')
			
		command = ['umount ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)
		command = ['umount ' + fromDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)

		command = ['dc3dd if=' + fromDiskDrive[0] + ' of='+ toDiskDrive[0]]
		run(command, shell=True, stdout=PIPE)
		self.logEventinView("Clonacion Lista")

		self.logEventinView("Montando dispositivos...")
		command = ['mkdir ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)
		command = ['mount --options=ro ' + toDiskDrive[0] + ' ' + toDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)


		command = ['mkdir ' + fromDiskDrive[1]]
		run(command, shell=True, stdout=PIPE)
		command = ['mount --options=ro ' + fromDiskDrive[0] + ' ' + fromDiskDrive[1]]
		print(command)
		run(command, shell=True, stdout=PIPE)

		self.logEventinView("Listo")
		return


	#getters and setters
	def get_currentToCloneItem(self):
		return self._currentToCloneItem

	def set_currentToCloneItem(self, currentToCloneItem):
		self._currentToCloneItem = currentToCloneItem

	def get_currentFromCloneItem(self):
		return self._currentFromCloneItem

	def set_currentFromCloneItem(self, currentFromCloneItem):
		self._currentFromCloneItem = currentFromCloneItem

	def get_allDrivesList(self):
		return self._allDrivesList

	def set_allDrivesList(self, allDrivesList):
		self._allDrivesList = allDrivesList


if __name__ == "__main__":
	app = QtWidgets.QApplication([])
	window = MainWindow()
	window.show()
	app.exec_()

