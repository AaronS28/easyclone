# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'EasyClone.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(426, 519)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.AcceptButton = QtWidgets.QPushButton(self.centralwidget)
        self.AcceptButton.setGeometry(QtCore.QRect(10, 410, 401, 34))
        self.AcceptButton.setObjectName("AcceptButton")
        self.FromCloneList = QtWidgets.QListWidget(self.centralwidget)
        self.FromCloneList.setGeometry(QtCore.QRect(10, 50, 151, 221))
        self.FromCloneList.setObjectName("FromCloneList")
        self.ToCloneList = QtWidgets.QListWidget(self.centralwidget)
        self.ToCloneList.setGeometry(QtCore.QRect(260, 50, 151, 221))
        self.ToCloneList.setObjectName("ToCloneList")
        self.LogTextView = QtWidgets.QTextEdit(self.centralwidget)
        self.LogTextView.setGeometry(QtCore.QRect(10, 310, 401, 91))
        self.LogTextView.setObjectName("LogTextView")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(60, 20, 41, 18))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(320, 20, 51, 18))
        self.label_2.setObjectName("label_2")
        self.MakeImgCheckBox = QtWidgets.QCheckBox(self.centralwidget)
        self.MakeImgCheckBox.setGeometry(QtCore.QRect(10, 280, 131, 22))
        self.MakeImgCheckBox.setObjectName("MakeImgCheckBox")
        self.RefreshButton = QtWidgets.QPushButton(self.centralwidget)
        self.RefreshButton.setGeometry(QtCore.QRect(10, 450, 401, 34))
        self.RefreshButton.setObjectName("RefreshButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.AcceptButton.setText(_translate("MainWindow", "Aceptar"))
        self.label.setText(_translate("MainWindow", "Origen"))
        self.label_2.setText(_translate("MainWindow", "Destino"))
        self.MakeImgCheckBox.setText(_translate("MainWindow", "Hacer Imagen"))
        self.RefreshButton.setText(_translate("MainWindow", "Refrescar Listas"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

